<cfsilent>
<!--- 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!! ATTENTION !!! ATTENTION !!! ATTENTION !!! ATTENTION !!! ATTENTION !!!

The file being modified by mdeangelis@jsi.com
Please not modify without first coordinating with me: x5261

!!! ATTENTION !!! ATTENTION !!! ATTENTION !!! ATTENTION !!! ATTENTION !!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--->
</cfsilent>

<cfsilent>
<!---
*******************************************************************************
/GitTest/index.cfm

home page for GitLab proof of concept

-------------------------------------------------------------------------------
Modification History (LIFO)
-------------------------------------------------------------------------------
07.12.17+- mdeangelis - GITTEST-1 - Demonstrate how we make software changes
04.17.17+- mdeangelis - GITTEST-2 - Fix the thing
05.15.17 - mdeangelis - added note about when page loaded on server
05.08.17 - md/dl - GitLab proof of concept
*******************************************************************************
--->
<cfset dt = now()/><!--- 05.15.17 - mdeangelis - add --->
</cfsilent>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>GitLab Test</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="Content-Language" content="en-us" />
<meta name="ROBOTS" content="ALL" />
<meta name="Copyright" content="Copyright <cfoutput>#dateFormat(dt,'yyyy')#</cfoutput> John Snow, Inc." />
<meta http-equiv="imagetoolbar" content="no" />
<meta name="MSSmartTagsPreventParsing" content="true" />
<meta name="Rating" content="General" />
<meta name="revisit-after" content="5 Days" />
<meta name="doc-class" content="Living Document" />

<script src="/Managed/js/gittest.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" media="screen" href="/css/gittest.css" />

</head>

<body id="_root_">

<div id="tagline_wrapper"><!-- BEGIN: tagline wrapper-->
    
   <h1>GitTest Home Page</h1>

   <!--- - - - - - - - - - - - - - - - - - - - --->
   <!--- 05.15.17 - mdeangelis - add section   --->
   <!--- - - - - - - - - - - - - - - - - - - - --->
   <cfoutput>
   <p>This page was re-generated on server at #dateFormat(dt,'medium')# #timeFormat(dt,'medium')#</p>
   <p>This is another paragraph</p><!--- 07.12.17+- mdeangelis - GITTEST-1 - add paragraph --->
   </cfoutput>
   <!--- - - - - - - - - - - - - - - - - - - - --->

</div><!-- END: tagline wrapper-->

</body>
</html>