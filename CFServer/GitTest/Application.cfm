<!---
********************************************************************************  
/GitTest/Application.cfm

This script is used for used for GitLab proof of concept

IMPORTANT:  Please do not check in with hard-coded site naming.

-------------------------------------------------------------------------------
Modification History (LIFO)
-------------------------------------------------------------------------------
06.26.17+- mdeangelis - INTRANET-25 - Change code base to support QA instance
05.08.17 - md/dl - GitLab proof of concept
********************************************************************************  
--->

<cfapplication name="GitTest!!CF_INSTANCE!!" sessionmanagement="Yes" setclientcookies="Yes">

<!--- DO NOT MUCK WITH THIS ONE, ALSO MAKE SURE enablecfoutputonly="Yes" IS ALWAYS AFTER IT --->
<cfinclude template="/CFUTILITIES/FuseGuard.cfm">

<!--- This is a comment that I put in --->

<cfsetting enablecfoutputonly="Yes">

<!--- Structured coldfusion error handling --->
<cferror type="exception" template="/CFUTILITIES/Error/ErrorHandler.cfm">
<cferror type="request" template="/CFUTILITIES/Error/RequestErrorHandler.cfm">

<!--- Alias root segment: important, to switch between parallel and final website model, change /WEIInternet2 to /WEIInternet --->
<cfset application.alias = "/GitTest"> 

<!--- Temp Directories --->
<cfset application.TempDir = "/opt/web/cfmx/storage/temp/">

<!--- Application variable set-up and re-initialization detection--->
<cfset reinitializeApplicationVars = false/>

<cfif NOT isDefined("application.dummy")> 
   <cfset reinitializeApplicationVars = true/>
</cfif>

<cfif isDefined("application.reset")>
   <cfif application.reset>
      <cfset reinitializeApplicationVars = true/>
   </cfif>
</cfif>

<cfif reinitializeApplicationVars>
	<!--- The first time the application is run after a server restart it will
		execute this section, which defines application variables.  The application
		variables currently in use are:
		dsn - the data source name.  THIS DATA SOURCE SHOULD BE SPECIFIC TO THE INSTANCE
		AGAINST WHICH THIS CODE IS RUNNING (i.e. the test code should run against the test
		data source, etc.).
		dstype - the data source type
		USCountryID - the key to the country record for the US
		AllowanceTypeID - the account type for allowances (which require a different account number format)
		dummy - the last variable set, and also the variable tested for.  Testing for the last
		variable set ensures that we keep executing this section even if one of the intermediate
		statements hoses.
		Application variables time out after two days (of inactivity??) by default,
		but that just means this may get executed every couple of days - no big deal. --->

   <!--- 06.26.17+- mdeangelis - INTRANET-25 - replace:
	<cfif Find("DEV",Ucase(application.ApplicationName),1)  EQ 0 and
		   Find("TEST",Ucase(application.ApplicationName),1) EQ 0 and
		   Find("PROD",Ucase(application.ApplicationName),1) EQ 0>
   --->
	<cfif find("DEV",ucase(application.applicationName),1)  EQ 0 
	  and find("QA",ucase(application.applicationName),1)   EQ 0  
	  and find("TEST",ucase(application.applicationName),1) EQ 0  
	  and find("PROD",ucase(application.applicationName),1) EQ 0>
		<cfoutput><html>
			<head>
				<title>Application Error</title>
			</head>
			<body>
				<h1>The application has not been properly named.  Please report this error to helpdesk.</h1>
			</body>
		</html></cfoutput>
		<cfabort>
	</cfif>
	
	<cflock timeout="30" throwontimeout="Yes" name="application.applicationName" type="Exclusive">

		<cfif Find("DEV",Ucase(application.ApplicationName),1) GT 0>
			<cfset application.instance = "DEV"/>
         
         <!--- photo paths--->
         <cfset application.PhotoServer = "http://photos-dev.jsi.com/Photos"/>
         <cfset application.PhotoBasePath = application.PhotoServer>
         <cfset application.ThumbBasePath = "http://photos-dev.jsi.com/Thumbnails/"/>

      <!--- 06.26.17+- mdeangelis - INTRANET-25 - add QA --->
		<cfelseif find("QA",ucase(application.applicationName),1) GT 0>
			<cfset application.instance = "QA"/>
         
         <!--- photo paths--->
         <cfset application.PhotoServer = "http://photos-qa.jsi.com.jsi.com/Photos"/>
         <cfset application.PhotoBasePath = application.PhotoServer>
         <cfset application.ThumbBasePath = "http://photos-qa.jsi.com/Thumbnails/"/>

		<cfelseif Find("TEST",Ucase(application.ApplicationName),1) GT 0>
         <cfset application.instance = "TEST"/>

         <!--- photo paths--->
         <cfset application.PhotoServer = "http://photos-test.jsi.com/Photos"/>
         <cfset application.PhotoBasePath = application.PhotoServer>
         <cfset application.ThumbBasePath = "http://photos-test.jsi.com/Thumbnails/"/>

		<cfelseif Find("PROD",Ucase(application.ApplicationName),1) GT 0>
         <cfset application.instance = "PROD"/>

         <!--- photo paths--->
         <cfset application.PhotoServer = "http://photos.jsi.com/Photos"/>
         <cfset application.PhotoBasePath = application.PhotoServer/>
         <cfset application.ThumbBasePath = "http://photos.jsi.com/Thumbnails/"/>
		</cfif>	
		    
      <!--- IMPORTANT: these variables are set here for legacy reasons - use _common_funcs_vars for any new development --->
      <cfset application.dsn = "EVS" & application.instance/>
      <cfset application.dstype = "ODBC"/>

      <cfset application.cfErrorEmailTo = "delphi_lee@jsi.com"/>
      <cfset application.cfErrorEmailFrom = "intranetinfo@jsi.com"/>

		<cfquery datasource="#application.dsn#" dbtype="#application.dstype#" name="qGetErrorEmailRecipient">
			select txtPropertyValue 
           from evs.app_config
          where txtPropertyName = 'ERROR_MESSAGE_RECIPIENT'
		</cfquery>

      <cfloop query="qGetErrorEmailRecipient">
         <cfset application.cfErrorEmailTo = qGetErrorEmailRecipient.txtPropertyValue/>
      </cfloop>

		<cfquery datasource="#application.dsn#" dbtype="#application.dstype#" name="qGetErrorEmailSender">
			select txtPropertyValue 
           from evs.app_config
          where txtPropertyName = 'ERROR_MESSAGE_SENDER'
		</cfquery>

      <cfloop query="qGetErrorEmailSender">
         <cfset application.cfErrorEmailFrom = qGetErrorEmailSender.txtPropertyValue/>
      </cfloop>
      <!--- - - - - - - - - - - - - - - - - - - - - - - --->

      <!--- flag set to indicate that application variable initialization has been done --->
		<cfset application.dummy = "set"/>

      <!--- flag set to indicate that application variable re-initialization (on special request basis) has been done --->
      <cfset application.reset = false/>
</cflock>
</cfif>

<cfsetting enablecfoutputonly="No">