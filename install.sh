#!/bin/bash
# Deploys the GitTest Internet Application Components

# FUNCTIONS
# include common functions
. $HOME/scripts/includes.sh


utl_ask_yes_no() {
  if [ "$#" -eq 0 ]
    then
      echo -n "Sorry. Missing parameters (utl_ask_yes_no). "
      echo    " Check the parameters passed in the main script."
    exit 1
  fi
  # $1 is passed as a name not de-referenced value else
  # the stuff bellow ( eval "$1=$user_input" ) will not work

   # These parameters are local in scope ...
   local user_input=""
   local dummy=""
   local confirm_msg="Confirm"
   local exit_loop=0
   # overwrite confirm message if exists
   if [ -n "$2" ]
    then
    confirm_msg=$2
   fi

   while [ "$exit_loop" -eq 0 ]
     do
     echo -n "${confirm_msg} (Y/N) [$user_input] =>"
     read dummy
     if [ -n "$dummy" ]
      then
        user_input=$dummy
     fi
     # convert to upper-case
     user_input=`echo "$user_input" | tr "[a-z]" "[A-Z]"`
     case "$user_input" in
        "N" | "NO" | "YES" | "Y" )
         #echo "You have selected '${user_input}' as an answer ..."
         # get only first character if 'YES/NO' entered
         user_input=`echo "$user_input" | cut -c1`
         exit_loop=1
        ;;
        *)
          echo "Make up your Mind. Don't like '${user_input}' as an answer ..."
          exit_loop=0
        ;;
     esac
   done
   # assign the new value to the passed parameter
   eval "$1=$user_input"
   return 1
}



# get arguments for cf install
get_cf_args()
{
	# Get the arguments needed to run the cf install
	done=0
	dummy=""
}

# prompt user to clear the /tmp/DISTRO from the disk drive. Useful particualry if releasing
# intranet/web/GitTest distro on the same server with two different users.
clearTmpDistro () {
    done=0
    dummy=""
    echo ""
    aok=""
    while [ $done -eq 0 ]
      do
        echo -n "Clear Temporary Code at ${thisdir} ? (y/n)"
        read aok
        # convert to upper-case
        aok=`echo "$aok" | tr "[a-z]" "[A-Z]"`
        case $aok in
             "N" | "NO" | "YES" | "Y" )
             # get only first character if 'YES/NO' entered
             aok=`echo "$aok" | cut -c1`
             done=1;
          ;;
        *)
        echo ok, please try again ...
        ;;
        esac
    done

    if [ "${done}" -eq 1 ] &&  [ "${aok}" = "Y" ] 
      then
        echo "rm -r ${thisdir}"
        rm -r ${thisdir}
    fi

}

# searches from a base, finds a case insensitive match and renames files
RenameFile () {
  if [ "$#" -ne 3 ]
	then
		echo "Sorry. Need three parameters to rename a file"
		exit 1
  fi
  # pass files that are sane and not REGEX i.e "*"
  local this_base=$1
  local this_file=$2
  local this_new_file=$3
  
   
   # find all files (case insensitive) of type file then rename the file in the same path (see dirname)
   # also don't care to rename a valid file if already exist
   for i in `find ${this_base} \( \( -type f -iname "${this_file}" \) -a \( -type f ! -name "${this_new_file}" \) \) -print`; \
    do mv ${verbose} $i `dirname $i`/${this_new_file}; \
   done
}

InstallCf()
{         
    if [ ! -d ${cfBasePath} ]  ||  [ ! -x ${cfBasePath} ]
      then
        echo "Fatal Error, base directory ${cfBasePath} does not exist or can not access it ... Bailing Out ..."
        exit 1
    fi
    if [ ! -d ${cfGitLabAppsPath} ] || [ ! -w ${cfGitLabAppsPath} ]
      then
        echo "Fatal Error, can not access base directory ${cfGitLabAppsPath} for writing ... Bailing Out ..."
        exit 1
    fi
	
	echo "Removing Old Application files ... ${thisCfModulePath}/ "
      rm -r ${verbose} ${thisCfModulePath}/
	
    if [ ! -d ${thisCfModulePath} ]
      then
        mkdir ${verbose} ${thisCfModulePath}
    fi 
    	

    # get rid of trailing '/' if any
    local tmpThisDir=`echo ${thisdir%/}`
    
    echo "Storing Application files ... ${thisCfModulePath}/"
      cp -r ${verbose} ${tmpThisDir}/CFServer/GitLab/* ${thisCfModulePath}/
    echo "Setting directory permissions ...${thisCfModulePath}/"
      echo "find ${thisCfModulePath}/* -type d -exec chmod ${verbose} 0750 {} \;"	
      find ${thisCfModulePath}/* -type d -exec chmod ${verbose} 0750 {} \;
    echo "Setting file permissions ...${thisCfModulePath}/"
      echo "find ${thisCfModulePath}/* -type f -exec chmod ${verbose} 0640 {} \;"	
      find ${thisCfModulePath}/* -type f -exec chmod ${verbose} 0640 {} \;   
   echo "Renaming invalid ${applicationCfmSearchTok} files to Linux accepted ${applicationCfmValidTok}"
       RenameFile $thisCfModulePath $applicationCfmSearchTok $applicationCfmValidTok
       
    echo "Assigning Proper values to ${thisCfModuleConfigFile} ..."
    echo "sed --in-place 's/!!CF_INSTANCE!!/${cfInstance}/g
                          s/!!PARALLELRUN_LIVENULL!!/${parallelNull}/g' ${thisCfModuleConfigFile}"
    sed --in-place "s/!!CF_INSTANCE!!/${cfInstance}/g
                    s/!!PARALLELRUN_LIVENULL!!/${parallelNull}/g" ${thisCfModuleConfigFile}

 
    echo ""
    echo "Done with InstallCf() ..."
    echo ""
}

InstallIntranet()
{
    echo ""
    echo "Nothing to do for InstallIntranet() ..."
    echo ""
}

InstallWeb()
{
    echo ""
    echo "Nothing to do for InstallWeb() ..."
    echo ""
}

InstallGitLab()
{
    if [ ! -d ${gitLabBasePath} ]
      then
        mkdir ${verbose} ${gitLabBasePath}
    fi 
	
    if [ ! -d ${gitLabBasePath} ]  ||  [ ! -x ${gitLabBasePath} ]
      then
        echo "Fatal Error, base directory ${gitLabBasePath} does not exist or can not access it ... Bailing Out ..."
        exit 1
    fi
	
    if [ ! -d ${gitLabCfAppsPath} ]
      then
        mkdir ${verbose} ${gitLabCfAppsPath}
    fi 
	
    if [ ! -d ${gitLabCfAppsPath} ] || [ ! -w ${gitLabCfAppsPath} ]
      then
        echo "Fatal Error, can not access base directory ${gitLabCfAppsPath} for writing ... Bailing Out ..."
        exit 1
    fi
	
    if [ ! -d ${gitLabAppsPath} ]
      then
        mkdir ${verbose} ${gitLabAppsPath}
    fi 
	
    if [ ! -d ${gitLabAppsPath} ] || [ ! -w ${gitLabAppsPath} ]
      then
        echo "Fatal Error, can not access base directory ${gitLabAppsPath} for writing ... Bailing Out ..."
        exit 1
    fi

    # get rid of trailing '/' if any
    local tmpThisDir=`echo ${thisdir%/}`
    
    echo "Storing GitLab Internet Cold Fusion files ... "
      cp -r ${verbose} ${tmpThisDir}/WebServer/CFApps/* ${thisGitLabCfModule}/
    
	rm -v ${thisGitLabModule}/GitLab/index.htm
	
	echo "Storing GitLab Internet Static files ... "
      cp -r ${verbose} ${tmpThisDir}/WebServer/Docs/* ${thisGitLabModule}/
 	echo "Replacing !!PARALLELRUN_LIVENULL!! with ${parallelNull} inside ${thisGitLabModule}/GitLab/index.htm ..."
	sed --in-place "s;!!PARALLELRUN_LIVENULL!!;${parallelNull};g" ${thisGitLabModule}/GitLab/index.htm
	
    echo "Setting directory permissions ..."
      echo "find ${thisGitLabCfModule} -type d -exec chmod ${verbose} ${thisGitLabDirPerms} {} \;"	
      find ${thisGitLabCfModule} -type d -exec chmod ${verbose} ${thisGitLabDirPerms} {} \;
	echo "find ${thisGitLabModule} -type d -exec chmod ${verbose} ${thisGitLabDirPerms} {} \;"	
      find ${thisGitLabModule} -type d -exec chmod ${verbose} ${thisGitLabDirPerms} {} \;

    echo "Setting file permissions ..."
      echo "find ${thisGitLabCfModule} -type f -exec chmod ${verbose} ${thisGitLabFilePerms} {} \;"	
      find ${thisGitLabCfModule} -type f -exec chmod ${verbose} ${thisGitLabFilePerms} {} \;
	echo "find ${thisGitLabModule} -type f -exec chmod ${verbose} ${thisGitLabFilePerms} {} \;"	
      find ${thisGitLabModule} -type f -exec chmod ${verbose} ${thisGitLabFilePerms} {} \;
    
    local ASK_INSTALL_APACHE=
    utl_ask_yes_no ASK_INSTALL_APACHE "Install Apache Config File"
    if [ "$ASK_INSTALL_APACHE" = "Y" ]
      then
        InstallApache
    else 
        echo " You chose not to install Apache Config File ..."
    fi


    echo ""
    echo "Done with InstallGitLab() ..."
    echo ""
}


InstallApache()
{
    # get rid of trailing '/' if any
    local tmpThisDir=`echo ${thisdir%/}`

	##  Copy from CVs to something else if not the same
	if [ "${thisApacheConfigFileCvs}" != "${thisApacheConfigFile}" ]; then
	  mv ${tmpThisDir}/conf/${thisApacheConfigFileCvs} ${tmpThisDir}/conf/${thisApacheConfigFile}
	fi
	
    echo "Assigning Proper values to ${thisApacheConfigFile} ..."
    echo "sed 's/!!WWWUSER!!/${thisGitLabUser}/g
	           s/!!WWWIP!!/${thisGitLabIp}/g
               s;!!WWWDNS!!;${thisGitLabDnsEntry};g
			   s;!!WWWDOMAIN!!;${thisGitLabDomain};g
			   s/!!DEVTSTNULLCOMMENT!!/${devtstnullcomment}/g
			   s/!!PARALLELRUN_LIVENULL!!/${parallelNull}/g
               s/!!COOKIE_DOMAIN!!/${thisCookieDomain}/g
               s/!!COOKIE_NAME!!/${thisCookieName}/g
               s/!!COOKIE_EXPIRES!!/${thisCookieExpires}/g' ${tmpThisDir}/conf/${thisApacheConfigFile} > ${tmpThisDir}/conf/${thisTmpApacheConfigFile}"

    sed "s/!!WWWUSER!!/${thisGitLabUser}/g
	     s/!!WWWIP!!/${thisGitLabIp}/g
         s;!!WWWDNS!!;${thisGitLabDnsEntry};g
		 s;!!WWWDOMAIN!!;${thisGitLabDomain};g
		 s/!!DEVTSTNULLCOMMENT!!/${devtstnullcomment}/g
		 s/!!PARALLELRUN_LIVENULL!!/${parallelNull}/g
         s/!!COOKIE_DOMAIN!!/${thisCookieDomain}/g
         s/!!COOKIE_NAME!!/${thisCookieName}/g
         s/!!COOKIE_EXPIRES!!/${thisCookieExpires}/g" ${tmpThisDir}/conf/${thisApacheConfigFile} > ${tmpThisDir}/conf/${thisTmpApacheConfigFile}

    echo " "
    echo "Setting ownership and restarting Apache"
    echo "if this step fails you MUST rerun 'release.sh' and 'install.sh'!!"
    echo "When prompted, enter the ${thisGitLabUser} password"

    echo "sudo mv ${tmpThisDir}/conf/${thisTmpApacheConfigFile} ${thisApacheConfigDir}/${thisApacheConfigFile} && sudo chown root:root ${thisApacheConfigDir}/${thisApacheConfigFile} && sudo ${thisApacheInit} restart"
    sudo mv ${tmpThisDir}/conf/${thisTmpApacheConfigFile} ${thisApacheConfigDir}/${thisApacheConfigFile} && sudo chown root:root ${thisApacheConfigDir}/${thisApacheConfigFile} && sudo ${thisApacheInit} restart
    
    echo ""
    echo "Done with InstallApache() ..."
    echo ""
}

usage()
{
	echo "Usage: $0 [cf|wei] [dev|tst|prd] (verbose) (parallel)"
	echo "Installs the latest build of the GitLab Internet Application Components"
	echo "The specified instance MUST match the one that runs on the"
	echo "machine where this is being executed"
	exit 1
}

checkUser()
{
  local user=$1
  local me=`whoami`
  if [ "$me" != "$user" ]
	then
	 echo "This script must be run as $user"
	 exit 1
  fi
}

#MAIN CODE
echo "$0 $1 $2 starting at `date`"

action="${1:-dummy}"
devtstprd="${2:-dummy}"
verboseTok="${3:-dummy}"
parallelTok="${4:-dummy}"

# convert to upper-case
verboseTok=`echo "$verboseTok" | tr "[a-z]" "[A-Z]"`
parallelTok=`echo "$parallelTok" | tr "[a-z]" "[A-Z]"`
# Overwrite here for NOW.
# parallelTok="PARALLEL"
parallelTok=""

verbose=
# like to see verbose output of shell operations, else leave blank
if [ "$verboseTok" = "VERBOSE" ]
  then 
  verbose="-v"
fi

parallelNull=
# like to see if we are running this as a parallel application, else leave blank
if [ "$parallelTok" = "PARALLEL" ]
  then 
  parallelNull="2"
fi

# CFMX7 in Linux needs application.cfm to be called Application.cfm
# will search  case insensitive and replace name
applicationCfmSearchTok="application.cfm"
applicationCfmValidTok="Application.cfm"
# Parameters
cfBasePath=/opt/web/cfmx
cfGitLabAppsPath=${cfBasePath}/webapps/cfusion

## To remove post going live ${parallelNull} or simply run script without 4th parameter PARALLEL
thisCfModulePath=${cfGitLabAppsPath}/GitLab${parallelNull}
# configuration file for the application (used for SED)
thisCfModuleConfigFile=${thisCfModulePath}/Application.cfm
thisCfUser=tomcat

# Internet/Web/GitLab 
gitLabBasePath=/home/worlded/website${parallelNull}
gitLabCfAppsPath=${gitLabBasePath}/CFApps
gitLabAppsPath=${gitLabBasePath}/Docs
thisGitLabCfModule=${gitLabCfAppsPath}
thisGitLabModule=${gitLabAppsPath}
thisGitLabDirPerms="0755"
thisGitLabFilePerms="0644"
# CHANGE ME HERE for multiple Apache instances, WWW=apache4
thisApacheConfigDir="/etc/apache4/vhosts.d"
thisApacheInit="/etc/init.d/apache4"

# Cvs Repository may have different filename per release, see InstallApache
thisApacheConfigFileCvs="worlded.conf"
thisApacheConfigFile="worlded${parallelNull}.conf"
# used because of sudo rules. It would have been easier to do sed in-place 
thisTmpApacheConfigFile="${thisApacheConfigFile}.tmp"
thisGitLabUser=gitlab
thisGitLabDnsEntry=
thisGitLabDomain=
thisGitLabIp=

# Cookie parameters assigned based on dev/test/prod
thisCookieDomain=
thisCookieName=
# if needed differently on dev/test/prod, nove variable assignemnt there
thisCookieExpires="30 years"



# Change to the directory with this script in it
thisproc=`basename $0`
thisdir=`echo $0 | sed "s/\(.*\)$thisproc/\1/"`
echo "Changing directory to ${thisdir} ..."
cd $thisdir

# You gotta specify an instance so we know what we're doing
instance=$devtstprd
instancecheck
cfInstance=
if [ $instance = "prd" ]
then
    cfInstance="Prod"
    devtstnull=""
	if [ "${parallelNull}" = "" ]; then
	  devtstnullcomment=""  
	else 
	  devtstnullcomment="#"  
	fi
    # change me when live to www.worlded.org
    thisGitLabDnsEntry="www${parallelNull}.gitlab.org"
    thisCookieName="Apache"
	 thisGitLabIp="172.16.251.203"
else
    devtstnull=$devtstprd
	devtstnullcomment="#"
    if [ $instance = "tst" ]
	then
        cfInstance="Test"
        thisGitLabDnsEntry="wei-test${parallelNull}.worlded.org"
        thisCookieName="Apache"
		thisGitLabIp="172.16.251.103"
    else
        cfInstance="Dev"
        thisGitLabDnsEntry="wei-dev${parallelNull}.worlded.org"
        thisCookieName="Apache"
		thisGitLabIp="172.16.43.143"
    fi
fi

# change me if generic .worlded.org needed
thisCookieDomain=".worlded.org"
thisGitLabDomain="worlded.org"


# perform instance- and install-type-specific actions
# the action specifies which bit we are actually installing
case "$action" in
	"cf")
            checkUser $thisCfUser
		get_cf_args
		InstallCf
		echo ""
		echo "Done with 'cf' deployment ..."
		echo ""
	
		;;
	"intranet")
            checkUser $thisGitLabUser
		InstallIntranet
		echo ""
            echo "Done with 'intranet' deployment ..."
            echo ""

		;;
	"web")
            checkUser $thisGitLabUser
		InstallWeb
		echo ""
        	echo "Done with 'web' deployment ..."
        	echo ""
	
		;;
	"wei")
            checkUser $thisGitLabUser
		InstallGitLab
		echo ""
        	echo "Done with 'wei' deployment ..."
        	echo ""
	
		;;	
	*)
		usage
		;;
esac

clearTmpDistro 

echo "$0 completed at `date`"

