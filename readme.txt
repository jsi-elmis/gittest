----------------------------------------------------------------------------------------------------
Please enter you changes LIFO (last in, first out) - include date mm.dd.yy, your userid, bugzilla
reference number (if applicable), a brief description of the change, and be sure to include any
special notes regarding dependencies or installation requirements.  For updated code, include the
new CVS revision number.
----------------------------------------------------------------------------------------------------

Updating the Readme for test.

07.12.17+- mdeangelis - GITTEST-1 - Demonstrate how we make software changes
-- Important: depends on (non-closed) items: GITTEST-2
Updated Source
--------------
/GitTest/index.cfm ( new rev: 1.5 )

07.03.17 - mdeangelis - INTRANET-25 - Change code base to support QA instance
-- Important: depends on (non-closed) items: JOBSDB-17, JOBSDB-27, INTRANET-5, INTRANET-15, INTRANET-22, INTRANET-24, PHOTODB-89, COUNTRYOPS-1, NBO-1, SKILLSDB-2
-- Deployment: Run SQL / follow instructions in /Intranet3/Setup/IntranetRedesign/SQL/EnhINTRANET-25.sql
-- Important: this change spans multiple CVS modules - this note will be included in each readme log
Added Source
------------
/Intranet3/Setup/IntranetRedesign/SQL/EnhINTRANET-25.sql
Updated Source
--------------
/CFUtilities/Utilities/Error/ErrorHandler.cfm ( new rev: 1.21 )
/Consultant/sprod/appvars.cfm ( new rev: 1.6.2.18 )
/Consultant/sprod/consultants/data.cfm ( new rev: 1.6.2.4 )
/Consultant/sprod/employees/notsupported.cfm ( new rev: 1.1.2.4 )
/EVS3/Application.cfm ( new rev: 1.5 )
/EVS3/SendEmailAttachTemplate.cfm ( new rev: 1.2 )
/EVS3/SendEmailTemplate.cfm ( new rev: 1.2 )
/EVS3/Inc/Common/_inc_common_init_action_handler.cfm ( new rev: 1.5 )
/EVS3/Inc/Common/_inc_header2_logo.cfm ( new rev: 1.4 )
/EVS3/Inc/RVR_ReviewVouchers/_inc_header.cfm ( new rev: 1.2 )
/GitTest/Application.cfm ( new rev: 1.3 )
/JSIIntranet/Application.cfm ( new rev: 1.29 )
/JSIIntranet/CompareEmpDirSvcs.cfm ( new rev: 1.4 )
/JSIIntranet/admin/_inc_PostingFrame_admin_alert.cfm ( new rev: 1.4 )
/JSIIntranet/AppConfig/Inc/_inc_header.cfm ( new rev: 1.5 )
/JSIIntranet/Archive/Application.cfm ( new rev: 1.12 )
/JSIIntranet/Archive/DocumentSetup.cfm ( new rev: 1.16 )
/JSIIntranet/CountryOps/index.cfm ( new rev: 1.11 )   
/JSIIntranet/CountryOps/CorporateDocRequests/UpdateAction.cfm ( new rev: 1.24 )
/JSIIntranet/CountryOps/Inc/_inc_header.cfm ( new rev: 1.10 )
/JSIIntranet/FieldUsers/Inc/_inc_header.cfm ( new rev: 1.8 ) 
/JSIIntranet/Harness/NewUserProvision.cfm ( new rev: 1.5 ) 
/JSIIntranet/Harness/ProvisioningPhase2/_inc_common_scenario_group_body_logo_nav.cfm ( new rev: 1.2 ) 
/JSIIntranet/Harness/ProvisioningPhase2/_inc_common_scenario_group_setup.cfm ( new rev: 1.3 ) 
/JSIIntranet/Inc/_inc_common_email_debug_intercept.cfm ( new rev: 1.2 ) 
/JSIIntranet/Inc/CommonFuncs/_common_funcs_photo.cfm ( new rev: 1.16 ) 
/JSIIntranet/Inc/QuickLinks/header.cfm ( new rev: 1.6 )  
/JSIIntranet/NBOs/EditRFPAction.cfm ( new rev: 1.26 ) 
/JSIIntranet/NBOs/Header.cfm ( new rev: 1.28 ) 
/JSIIntranet/RoleManager/_inc_header.cfm ( new rev: 1.5 ) 
/JSIIntranet/Search/_inc_header.cfm ( new rev: 1.5 ) 
/JSIIntranet/Services/AutoCheckService.cfm ( new rev: 1.3 ) 
/JSIIntranet/Services/CrawlAnalysisService.cfm ( new rev: 1.2 ) 
/JSIIntranet/Services/DataConsistencyService.cfm ( new rev: 1.5 ) 
/JSIIntranet/SkillsDatabase/Inc/_inc_get_person_data_function.cfm ( new rev: 1.16 )
/JSIIntranet/SubscriptionService/IntranetDigest/_inc_header.cfm ( new rev: 1.2 )
/JSIIntranet/SubscriptionService/IntranetDigest/_inc_implementation.cfm ( new rev: 1.5 )
/JSIIntranet/WebsiteContent/_inc_common_setup.cfm ( new rev: 1.37 ) 
/JSIIntranet/WebsiteContent/_inc_header.cfm ( new rev: 1.11 ) 
/JSIJobs/Application.cfm ( new rev: 1.15 ) 
/JSIJobs/appvars.cfm ( new rev: 1.3 ) [deprecated]
/JSIJobs/admin/_inc_common_email_debug_intercept.cfm ( new rev: 1.11 ) 
/JSIJobs/admin/Application.cfm ( new rev: 1.18 ) 
/JSIJobs/admin/edit_termination_date_process.cfm ( new rev: 1.2 ) 
/JSIJobs/admin/manage_candidates.cfm ( new rev: 1.38 ) 
/JSIJobs/apply_online/Application.cfm ( new rev: 1.11 ) 
/JSIJobs/apply_online/appvars.cfm ( new rev: 1.3 ) [deprecate]
/JSIJobs/apply_online/withdraw_application_action.cfm ( new rev: 1.4 )
/JSIJobs/jsiapplicants/Application.cfm ( new rev: 1.13 )
/JSIApi/Application.cfm ( new rev: 1.3 )
/JSIInternet/Application.cfm ( new rev: 1.25 )
/JSIInternet/Payments/Famplan/Inc/_famplan_settings.cfm ( new rev: 1.2 )
/JSIProjects/application.cfm ( new rev: 1.16 )
/WEIInternet/Application.cfm ( new rev: 1.10 )
/WEIInternet/inc/common/_setup_display_base_object.cfm ( new rev: 1.4 )

05.15.17 - mdeangelis - added note about when page loaded on server
Updated Source
--------------
/GitTest/index.cfm ( new rev: 1.2, 1.3, 1.4 )

05.08.17 - md/dl - GitLab proof of concept - created this file
